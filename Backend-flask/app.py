from unittest import result
from flask import Flask, request
import pickle

from sklearn import pipeline
import cloudpickle
import pandas as pd
import numpy as np
import sys

import sklearn.neighbors._base
sys.modules['sklearn.neighbors.base'] = sklearn.neighbors._base
from missingpy import MissForest

import pickle

import time
from IPython.display import display, clear_output

import seaborn as sns

import numpy as np

import matplotlib.pyplot as plt

import pandas as pd

from tabulate import tabulate

from collections import Counter
from imblearn.over_sampling import SMOTE, SMOTENC 
from imblearn.under_sampling import RandomUnderSampler, TomekLinks, EditedNearestNeighbours
from imblearn.combine import SMOTEENN

from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold
from sklearn.preprocessing import MinMaxScaler, LabelEncoder, FunctionTransformer
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.feature_selection import RFE
from sklearn.compose import ColumnTransformer
from imblearn.pipeline import Pipeline

from catboost import CatBoostClassifier, Pool, cv
from catboost.utils import get_gpu_device_count

np.set_printoptions(threshold=np.inf)

pd.set_option("display.max_columns", None)

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello World!'

@app.route('/predict', methods=['POST','GET'])
def predict():
    model = cloudpickle.load(open('enn_undersampling_split_4_with_mean.pickle', 'rb'))
    body = request.json
    bodyJson = body['body']
    print(body, file=sys.stderr)
    print(bodyJson, file=sys.stderr)
    dfJson = pd.DataFrame(bodyJson, index=['i',])
    print(dfJson, file=sys.stderr)
    dfJson.reset_index(drop=True, inplace=True)
    print(dfJson, file=sys.stderr)
    prediction = model['CatBoost']['grid_search'].predict(dfJson)
    probability = model['CatBoost']['grid_search'].predict_proba(dfJson)
    result = {}
    result['prediction_result'] = str(prediction[0])
    result['probability'] = max(probability[0]) * 100 
    return result

if __name__ == "__main__":
    app.run(debug=True)