import React, { Component, useState } from 'react'
import Highcharts from "highcharts";
import HighchartsMap from "highcharts/modules/map";
import HighchartsReact from 'highcharts-react-official';
import mapData from "@highcharts/map-collection/countries/id/id-all.geo.json";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';

const GeoChart =  (props) => {
    HighchartsMap(Highcharts);

    const dataGeoChart = props.dataDropoutGeoChart.data;

    const options = {
        chart: {
            map: mapData
        },
        title: {
            text: 'Peta Rasio Mahasiswa Drop Out dan Lulus di Indonesia'
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: "bottom"
            }
        },

        legend: {
            layout: 'horizontal',
            padding: 3,
            itemMarginTop: 5,
            itemMarginBottom: 5,
        },

        colorAxis: {
            min: 0
        },

        series: [{
                showInLegend:true,
                data: dataGeoChart,
                name: 'Rasio Mahasiswa Drop Out',
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                tooltip: {
                    valueSuffix: '%'
                }
            }]
    }
   

    return (
        <div className="container mt-10">
            <Box sx={{ minWidth: 550 }} pt={10} mt={10}>
                <FormControl sx={{ width: 550 }}>
                    <InputLabel id="demo-simple-select-label">Year</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={props.tahunSemester}
                        label="Tahun Ajaran"
                        onChange={props.handleDropdownChange}   
                    >
                        <MenuItem value={2015}>2015</MenuItem>
                        <MenuItem value={2016}>2016</MenuItem>
                        <MenuItem value={2017}>2017</MenuItem>
                        
                    </Select>
                </FormControl>
                {/* <Button variant="contained" onClick={props.handleButtonChange}>Persentase Dropout</Button> */}
            </Box>
            <HighchartsReact 
                highcharts={Highcharts}
                options={options}
                constructorType={'mapChart'}
            />
        </div>
    )
}

export default GeoChart;