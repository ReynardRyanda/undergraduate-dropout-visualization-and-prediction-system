import React, { Component, useState } from 'react'
import Chart from 'react-google-charts'
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';


const PieChart = (props) => {
    
    const options = {
        title: 'Status Mahasiswa S1 di Indonesia',
        
    }

    return (
        <div className='container'>
            <h2>Pie Chart</h2>
            <Box sx={{ minWidth: 550 }} pt={3}>
            <FormControl sx={{ width: 550 }}>
                <InputLabel id="demo-simple-select-label">Tahun Semester</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={props.tahunSemesterPieChart}
                    label="Tahun Ajaran"
                    onChange={props.handleTahunSemesterPieChartChange}   
                >
                <MenuItem value={20141}>2014/1</MenuItem>
                <MenuItem value={20142}>2014/2</MenuItem>
                <MenuItem value={20151}>2015/1</MenuItem>
                <MenuItem value={20152}>2015/2</MenuItem>
                <MenuItem value={20161}>2016/1</MenuItem>
                <MenuItem value={20162}>2016/2</MenuItem>
                <MenuItem value={20171}>2017/1</MenuItem>
                <MenuItem value={20172}>2017/2</MenuItem>
                <MenuItem value={20181}>2018/1</MenuItem>
                <MenuItem value={20182}>2018/2</MenuItem>
                <MenuItem value={20191}>2019/1</MenuItem>
                <MenuItem value={20192}>2019/2</MenuItem>
                <MenuItem value={20201}>2020/1</MenuItem>
                <MenuItem value={20202}>2020/2</MenuItem>
                <MenuItem value={20211}>2021/1</MenuItem>
                <MenuItem value={20212}>2021/2</MenuItem>
                </Select>
            </FormControl>
            </Box>
            <Chart
                width={1000}
                height={800}
                chartType="PieChart"
                data={props.dataDropoutPieChart}
                options={options}
            />
        </div>
    )
}

export default PieChart;