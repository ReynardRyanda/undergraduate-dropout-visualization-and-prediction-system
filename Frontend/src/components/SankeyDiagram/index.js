import React, { Component, useState } from 'react'
import Chart from 'react-google-charts'
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import Container from '@mui/material/Container';
import CircularProgress from '@mui/material/CircularProgress';

// ======================== CSS for Sankey ======================================
// #fb9a99
// 

var colors = ['#a6cee3', '#fdbf6f', '#fb9a99', '#a6cee3', '#a6cee3', '#a6cee3', '#a6cee3', '#a6cee3', '#a6cee3', '#b2df8a', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#a6cee3', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#a6cee3', '#a6cee3', '#a6cee3', '#fdbf6f', '#ceefb1', '#cab2d6', '#fdbf6f'];;

// var colors = ['#a6cee3', '#fdbf6f', '#fb9a99', '#a6cee3', '#a6cee3', '#a6cee3', '#a6cee3', '#a6cee3', '#a6cee3', '#b2df8a', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#a6cee3', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#fdbf6f', '#a6cee3', '#a6cee3', '#a6cee3', '#fdbf6f', '#ceefb1', '#cab2d6', '#fdbf6f', ];

// Sets chart options.
var options = {
  // forceIFrame: true,
  chartArea:{
    right:100,
  },
  chartArea: {
    width: '100%', 
    height: '100%'
  },
  sankey: {
      node: {
        label: {
          fontSize: 14,
        },
        iterations: 100,
        width: 6,
        height: 100,
        labelPadding: 6,     // Horizontal distance between the label and the node.
        // colors: colors,
        nodePadding: 16,
        interactivity: true,
        colors: colors,
      },
      link: {
          colorMode: 'gradient',
          width: 50,
          colors: colors
        }
      }
};
// ======================== end of CSS for Sankey ======================================


const SankeyChart = (props) => {
  
  return (
      <Container>
        <h2>Student Flow - Sankey Diagram</h2>
        <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
            {props.isFetchingSankey && <CircularProgress disableShrink  color="secondary" />} 
        </div>
        <Box sx={{ minWidth: 550 }} pt={3}>
          <FormControl sx={{ width: 550 }}>
            <InputLabel id="demo-simple-select-label">Year</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={props.tahunAjaran}
              label="Tahun Ajaran"
              onChange={props.handleDropdownChange}   
            >
              <MenuItem value={2015}>2015</MenuItem>
              <MenuItem value={2016}>2016</MenuItem>
              <MenuItem value={2017}>2017</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Chart
          width={1200}
          height={800}
          chartType="Sankey"
          loader={<div>Loading Chart</div>}
          data={props.dataDropoutFlow.data}
          options={options}
          rootProps={{ 'data-testid': '1' }}
        />
      </Container>
    )
}


export default SankeyChart