import React, { useRef, useEffect } from 'react';
import Chart from 'react-google-charts'

import Container from '@mui/material/Container';

var options = {
    bars: 'horizontal',
    width: 1000,
    isStacked: 'percent'
}

const BarChartLulusDo = (props) => {
    return (
        <Container fixed>
            <h2>Perbandingan Jumlah Mahasiswa Drop Out dan Lulus Tiap Tahun Angkatan</h2>
            <Chart
                chartType="BarChart"
                width="100%"
                height="400px"
                data={props.dataLulusDropoutBarChart.data}
                options={options}
            />
        </Container>
    )
}

export default BarChartLulusDo;