import React, { useRef, useEffect } from 'react';
import Chart from 'react-google-charts'
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Container from '@mui/material/Container';


var options = {
    bars: 'horizontal',
    width: 1000
}

const BarChart = (props) => {
    return (
        <Container fixed>
            <Box sx={{ minWidth: 550 }} pt={10} mt={10}>
                <h2>Perbandingan Jumlah Mahasiswa Drop Out Tiap Semester per Tahun Angkatan</h2>
                <FormControl sx={{ width: 550 }}>
                    <InputLabel id="demo-simple-select-label">Tahun/Semester</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={props.tahunBarChart}
                        label="Tahun Angkatan ke-"
                        onChange={props.handleTahunBarChartChange}   
                    >
                        <MenuItem value={2015}>2015</MenuItem>
                        <MenuItem value={2016}>2016</MenuItem>
                        <MenuItem value={2017}>2017</MenuItem>
                        
                    </Select>
                </FormControl>
            </Box>
            <Chart
                chartType="BarChart"
                width="100%"
                height="400px"
                data={props.dataDropoutBarChart.data}
                options={options}
            />
        </Container>
    )
}

export default BarChart;

// props.dataDropoutBarChart.data