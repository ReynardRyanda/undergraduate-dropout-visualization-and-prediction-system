import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import Container from '@mui/material/Container';
import InputLabel from '@mui/material/InputLabel';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import CircularProgress from '@mui/material/CircularProgress';

function DetailMahasiswa(){
    const params = useParams();
    const [dataDetailMahasiswa, setDataDetailMahasiswa] = useState({});
    const [isFetching, setIsFetching] = useState(true)
    
    const merantau_label = {
        1:true,
        0:false
    }
    
    const url = 'http://localhost:16200/api/'
    const getDataDetailMahasiswa = async () => {
        await axios
            .get(`${url}id_reg_pd/${params.id_reg}`)
            .then((response) => {
                response.data['id_reg_pd'] = params.id_reg
                response.data['sp'] = sp_label[response.data['sp']]
                setDataDetailMahasiswa(response.data)
            })
            .then((response) => {
                setTimeout(()=>{
                    setIsFetching(false)
                }, 2000)
            })
    }

    useEffect(() => {
        getDataDetailMahasiswa()
    }, []);

    const [prediksi, setPrediksi] = useState()
    const [persentasePrediksi, setPersentasePrediksi] = useState()
    const [checked, setChecked] = useState();

    const defaultValueMerantau = merantau_label[dataDetailMahasiswa.merantau]

    const changeMataKuliahData = (value, smt, id, key) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`kelas_smt_${smt}`][id][key] = value
        setDataDetailMahasiswa(tempData)
        console.log('berubah');
    }

    const changeNilaiIpSemesterData = (value, smt) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`ips_${smt}`] = value
        setDataDetailMahasiswa(tempData)
        console.log('berubah');
    }
    
    const changeNilaiIpkSemesterData = (value, smt) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`ipk_${smt}`] = value
        setDataDetailMahasiswa(tempData)
        console.log('berubah');
    }

    const changeSksSemesterData = (value, smt) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`sks_smt_${smt}`] = value
        setDataDetailMahasiswa(tempData)
        console.log('berubah');
    }

    const changeSksTotalSemesterData = (value, smt) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`sks_total_${smt}`] = value
        setDataDetailMahasiswa(tempData)
        console.log('berubah');
    }

    const addEmptyMatkul = (smt) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`kelas_smt_${smt}`].push({
            'nm_mk':'',
            'nilai_indeks':0
        })
        setDataDetailMahasiswa(tempData)
    }

    const deleteLastMatkul = (smt) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`kelas_smt_${smt}`].pop()
        setDataDetailMahasiswa(tempData)
    }

    const changeTanggalLahir = (newValue) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`tgl_lahir`] = newValue
        setDataDetailMahasiswa(tempData);
    };

    const changeTanggalMasukUniversitas = (newValue) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`tgl_masuk_sp`] = newValue
        setDataDetailMahasiswa(tempData);
    };

    const changeUniversitas = (newValue) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`nm_lemb`] = newValue.target.value
        setDataDetailMahasiswa(tempData);
    };

    const changeAgama = (newValue) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`agama`] = newValue.target.value
        setDataDetailMahasiswa(tempData);
    };

    const changeJenisKelamin = (newValue) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`jk`] = newValue.target.value
        setDataDetailMahasiswa(tempData);
    };

    const handleChecboxMerantau = (event) => {
        const tempData = {...dataDetailMahasiswa}
        tempData[`merantau`] = setChecked(event.target.checked);
      };

    console.log(defaultValueMerantau)
    console.log(defaultValueMerantau)
    console.log(defaultValueMerantau)

    // useEffect( () => {
    //     // console.log(dataDetailMahasiswa);
        
    //     const matkul_smt_1 = dataDetailMahasiswa['kelas_smt_1'].map(matkul => {
    //         if (matkul['nilai_indeks'] == '') {
    //             return {
    //                 ...matkul,
    //                 nilai_indeks: 0
    //             }
    //         } else {
    //             let nilai_indeks = parseFloat(matkul['nilai_indeks'])

    //             if (nilai_indeks > 4) nilai_indeks = 4;
    //             if (nilai_indeks < 0) nilai_indeks = 0;
    
    //             return {
    //                 ...matkul,
    //                 nilai_indeks
    //             }
    //         }
    //     })

    //     const matkul_smt_2 = dataDetailMahasiswa['kelas_smt_2'].map(matkul => {
    //         if (matkul['nilai_indeks'] == '') {
    //             return {
    //                 ...matkul,
    //                 nilai_indeks: 0
    //             }
    //         } else {
    //             let nilai_indeks = parseFloat(matkul['nilai_indeks'])

    //             if (nilai_indeks > 4) nilai_indeks = 4;
    //             if (nilai_indeks < 0) nilai_indeks = 0;
    
    //             return {
    //                 ...matkul,
    //                 nilai_indeks
    //             }
    //         }
    //     })

    //     const tempData = {...dataDetailMahasiswa}
    //     tempData['kelas_smt_1'] = matkul_smt_1
    //     tempData['kelas_smt_2'] = matkul_smt_2
    //     setDataDetailMahasiswa(tempData)
    // }
    // , [dataDetailMahasiswa])

    const predict = async () => {
        const dataPrediksiMahasiswa = {...dataDetailMahasiswa}

        const mapKelasSmtSatu = dataDetailMahasiswa.kelas_smt_1.map (item => 
            Object.values(item)[1]
        )

        dataPrediksiMahasiswa['kelas_smt_1'] = mapKelasSmtSatu

        const mapKelasSmtDua = dataDetailMahasiswa.kelas_smt_2.map (item => 
            Object.values(item)[1]
        )

        dataPrediksiMahasiswa['kelas_smt_2'] = mapKelasSmtDua

        dataPrediksiMahasiswa['merantau'] = dataPrediksiMahasiswa.merantau

        console.log(dataPrediksiMahasiswa);
        await axios
            .post(`${url}predict`, {...dataPrediksiMahasiswa})
            .then(response => {
                setPrediksi(response.data[0]['prediction_result']) //cari tau bentuk datanya gmn
                setPersentasePrediksi(response.data[0]['probability'].toFixed(2))
            }) 
    }

    const sp_label = {
        "756a14726f82a35075ded68b9c3981f6": 1,
        "fc9cfa53f6920bf94f8e17fb4ce6e682": 2,
        "fa835990a8e9f9d9f8713c5b58a37d03": 3,
        "92ed86751a69e75e5608f2226941971a": 4,
        "1fd17fc2fa3555052714ec8f2cff102e": 5,
        "588459882664388203e14c98748472d8": 6,
        "59cbf217f610fa74ac856fcfd62ce2ba": 7,
        "b62717456ec0fa8e42ad230361cab55d": 8,
        "9bdbe67e89fa43a30e9c5950ee01cac3": 9,
        "deb6e3bc9e0948995ebe7137b4bb1f17": 10,
        "06d67759db08c2f8072d6a2f1d4f7a7a": 11,
        "c5e493c39f6d25402918d9ad9f9be009": 12
    }


    return (
        <Container fixed>
            {/* <p>{params.id_reg}</p> */}
            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                {isFetching && <CircularProgress disableShrink  color="secondary" />} 
            </div>
            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                 <h2>Nama Mahasiswa: {dataDetailMahasiswa?.nm_pd}</h2>
            </div>
            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                <h2>NIM:  {dataDetailMahasiswa?.nipd}</h2>
            </div>
            <Stack spacing={3}>
                <Box sx={{ minWidth: 550 }} pt={3}>
                    <FormControl sx={{ width: 550 }}>
                        <InputLabel id="demo-simple-select-helper-label">Nama Lembaga</InputLabel>
                        <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="Nama Lembaga"
                        // value={sp_label[dataDetailMahasiswa?.sp]}
                        value={dataDetailMahasiswa?.sp ?? 1}
                        onChange={changeUniversitas}   
                        >
                            <MenuItem value={1}>Uxxxxxxxxxx Mxxxx Bxxxx</MenuItem>
                            <MenuItem value={2}>Uxxxxxxxxxx Mxxxxxxxxx Nxxxxxxxx Jxxxxxx</MenuItem>
                            <MenuItem value={3}>Uxxxxxxxxxx Mxxxxxxxxxxx Pxxx Dx Hxxxx</MenuItem>
                            <MenuItem value={4}>Uxxxxxxxxxx Sxxxxxx</MenuItem>
                            <MenuItem value={5}>Uxxxxxxxxxx Lxxxxxx</MenuItem>
                            <MenuItem value={6}>Uxxxxxxxxxx Sxxxx Kxxxx</MenuItem>
                            <MenuItem value={7}>Ixxxxxxx Ixxx Sxxxxx dxx Mxxxxxxxx Sxxxxx</MenuItem>
                            <MenuItem value={8}>Uxxxxxxxxxx Rxxxxxx Yxxxxxxxxx</MenuItem>
                            <MenuItem value={9}>Ixxxxxxx Txxxxxxxx Pxxxxx</MenuItem>
                            <MenuItem value={10}>Uxxxxxxxxxx Mxx Txxxxxxx</MenuItem>
                            <MenuItem value={11}>Uxxxxxxxxxx Mxxxxxxxxxxx Pxxxxxxx</MenuItem>
                            <MenuItem value={12}>Uxxxxxxxxxx Dxxxx Pxxxxxx</MenuItem>
                        </Select>
                    </FormControl>
                </Box>
                <div>
                <FormControl sx={{ width: 550 }}>
                        <InputLabel id="demo-simple-select-helper-label">Jenis Kelamin</InputLabel>
                        <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        label="Jenis Kelamin"
                        value={dataDetailMahasiswa?.jk ?? 1}
                        onChange={changeJenisKelamin}   
                        >
                            <MenuItem value={1}>Laki-laki</MenuItem>
                            <MenuItem value={0}>Perempuan</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <div>
                    <Box sx={{ minWidth: 550 }}>
                        <FormControl sx={{ width: 250 }}>
                            <InputLabel id="demo-simple-select-helper-label">Agama</InputLabel>
                            <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Agama"
                            value={dataDetailMahasiswa?.agama ?? 1}
                            onChange={changeAgama}   
                            >
                                <MenuItem value={1}>Islam</MenuItem>
                                <MenuItem value={2}>Kristen</MenuItem>
                                <MenuItem value={3}>Katolik</MenuItem>
                                <MenuItem value={4}>Hindu</MenuItem>
                                <MenuItem value={5}>Buddha</MenuItem>
                                <MenuItem value={6}>Khonghucu</MenuItem>
                                <MenuItem value={98}>Tidak ada data</MenuItem>
                                <MenuItem value={99}>Lainnya</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                </div>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <div>
                            <DesktopDatePicker
                            label="Tanggal Lahir"
                            inputFormat="MM/dd/yyyy"
                            value={dataDetailMahasiswa?.tgl_lahir}
                            onChange={changeTanggalLahir}
                            renderInput={(params) => <TextField {...params} />}>
                            </DesktopDatePicker>
                    </div>
                    <div>
                    <DesktopDatePicker
                            label="Tanggal Masuk Lembaga"
                            inputFormat="MM/dd/yyyy"
                            value={dataDetailMahasiswa?.tgl_masuk_sp}
                            onChange={changeTanggalMasukUniversitas}
                            renderInput={(params) => <TextField {...params} />}>
                            </DesktopDatePicker>
                    </div>
                </LocalizationProvider>
                <FormGroup>
                    <FormControlLabel 
                        control={<Checkbox defaultChecked />} 
                        checked={checked}
                        label="Merantau (apabila tempat kuliah Anda berbeda provinsi dengan tempat Anda tinggal)" 
                        onChange={handleChecboxMerantau}
                        
                        />
                </FormGroup>
            </Stack>
            <TableContainer component={Paper}>
                <h3>Data Semester 1</h3>
                <Stack direction="row" spacing={4}>
                        <TextField
                                required
                                id="outlined-required"
                                label="Nilai IP Semester 1"
                                InputLabelProps={{ shrink: true }}
                                onChange={({target : {value}}) => changeNilaiIpSemesterData(value, 1)}
                                value={dataDetailMahasiswa?.ips_1}
                        />
                        <TextField
                                required 
                                id="outlined-required"
                                label="Nilai IPK Semester 1"
                                InputLabelProps={{ shrink: true }}
                                value={dataDetailMahasiswa?.ipk_1}
                                onChange={({target : {value}}) => changeNilaiIpkSemesterData(value, 1)}
                        />
                        <TextField
                                required
                                id="outlined-required"
                                label="SKS diambil Semester 1"
                                InputLabelProps={{ shrink: true }}
                                value={dataDetailMahasiswa?.sks_smt_1}
                                onChange={({target : {value}}) => changeSksSemesterData(value, 1)}
                        />
                        <TextField
                                required
                                id="outlined-required"
                                label="SKS Total"
                                InputLabelProps={{ shrink: true }}
                                value={dataDetailMahasiswa?.sks_total_1}
                                onChange={({target : {value}}) => changeSksTotalSemesterData(value, 1)}
                        />
                </Stack>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell><b>Nama Mata Kuliah</b></TableCell>
                        <TableCell align="center"><b>Nilai indeks</b></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {dataDetailMahasiswa?.kelas_smt_1?.map((row,id) => (
                        <TableRow
                        key={id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row">
                        <TextField
                            required
                            id="outlined-required"
                            // type="search"
                            label=""
                            value={row.nm_mk}
                            onChange={({target : {value}}) => changeMataKuliahData(value, 1, id, 'nm_mk')}
                        />
                        </TableCell>
                        <TableCell align="center">
                        <TextField
                            required
                            id="outlined-required"
                            label=""
                            type="number"
                            step="0.1"
                            value={row.nilai_indeks}
                            onChange={({target : {value}}) => changeMataKuliahData(value, 1, id, 'nilai_indeks')}
                        />
                        </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                <Stack direction="row" spacing={4}>
                    <Button 
                        variant="contained"
                        onClick={() => addEmptyMatkul(1)}
                        // value={searchedName}
                        size="small"
                        color="primary"
                        style={{ marginTop: `15px` }}>Tambah Mata Kuliah</Button>
                    <Button 
                        variant="contained"
                        onClick={() => deleteLastMatkul(1)}
                        // value={searchedName}
                        size="small"
                        color="error"
                        style={{ marginTop: `15px` }}>Hapus Mata Kuliah</Button>
                </Stack>
            </TableContainer>
            <TableContainer component={Paper}>
                <h3>Data Semester 2</h3>
                <Stack direction="row" spacing={4}>
                    <TextField
                            required
                            id="outlined-required"
                            label="Nilai IP Semester 2"
                            InputLabelProps={{ shrink: true }}
                            value={dataDetailMahasiswa?.ips_2}
                            onChange={({target : {value}}) => changeNilaiIpSemesterData(value, 2)}
                    />
                    <TextField
                            required
                            id="outlined-required"
                            label="Nilai IPK Semester 2"
                            InputLabelProps={{ shrink: true }}
                            value={dataDetailMahasiswa?.ipk_2}
                            onChange={({target : {value}}) => changeNilaiIpkSemesterData(value, 2)}
                    />
                    <TextField
                            required
                            id="outlined-required"
                            label="SKS diambil Semester 2"
                            InputLabelProps={{ shrink: true }}
                            value={dataDetailMahasiswa?.sks_smt_2}
                            onChange={({target : {value}}) => changeSksSemesterData(value, 2)}
                    />
                    <TextField
                            required
                            id="outlined-required"
                            label="SKS Total"
                            InputLabelProps={{ shrink: true }}
                            value={dataDetailMahasiswa?.sks_total_2}
                            onChange={({target : {value}}) => changeSksTotalSemesterData(value, 2)}
                    />
                </Stack>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell><b>Nama Mata Kuliah</b></TableCell>
                        <TableCell align="center"><b>Nilai indeks</b></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {dataDetailMahasiswa?.kelas_smt_2?.map((row,id) => (
                        <TableRow
                        key={id}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row">
                        <TextField
                            required
                            id="outlined-required"
                            value={row.nm_mk}
                            onChange={({target : {value}}) => changeMataKuliahData(value, 2, id, 'nm_mk')}
                        />
                        </TableCell>
                        <TableCell align="center">
                        <TextField
                            required
                            id="outlined-required"
                            type="number"
                            step="0.1"
                            value={row.nilai_indeks}
                            onChange={({target : {value}}) => changeMataKuliahData(value, 2, id, 'nilai_indeks')}
                        />
                        </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                <Stack direction="row" spacing={4}>
                    <Button 
                        variant="contained"
                        onClick={() => addEmptyMatkul(2)}
                        // value={searchedName}
                        size="small"
                        color="primary"
                        style={{ marginTop: `15px` }}>Tambah Mata Kuliah</Button>
                    <Button 
                        variant="contained"
                        onClick={() => deleteLastMatkul(2)}
                        // value={searchedName}
                        size="small"
                        color="error"
                        style={{ marginTop: `15px` }}>Hapus Mata Kuliah</Button>
                </Stack>
            </TableContainer>
            <h3>Apakah Data Anda sudah valid?</h3>
            <Stack spacing={3} mt={2} mb={3}>
                <Button 
                        variant="contained"
                        onClick={() => predict()}
                        // value={searchedName}
                        size="small"
                        color="primary"
                        style={{ marginTop: `15px` }}>yes
                </Button>
                {prediksi == 0 && <Alert variant="filled" severity="success"><h1>Kami senang memberi tahu Anda bahwa algoritma kami tidak mendeteksi Anda untuk putus studi dengan persentase sebesar <b>{persentasePrediksi}%</b>. Anda disarankan untuk tetap belajar lebih giat.</h1></Alert>}
                {prediksi == 1 && <Alert variant="filled" severity="warning"><h1>Kami minta maaf untuk memberitahu Anda bahwa Anda mungkin berpotensi untuk putus studi dengan persentase sebesar <b>{persentasePrediksi}%</b>. Konsultasikan segera kepada orang tua Anda dan tetap belajar lebih giat.</h1></Alert>}
            </Stack>
            
        </Container>
       
    )
}

export default DetailMahasiswa;