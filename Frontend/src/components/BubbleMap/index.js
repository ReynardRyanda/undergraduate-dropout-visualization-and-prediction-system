import React, { Component, useState } from 'react'
// import '../../proj4.js'
// import proj4 from '../../proj4';
import Highcharts from "highcharts";
import HighchartsMap from "highcharts/modules/map";
import HighchartsReact from 'highcharts-react-official';
import map from "@highcharts/map-collection/countries/id/id-all.geo.json";

const BubbleMap = (props) => {
    HighchartsMap(Highcharts);
    // const dataBubbleMap = props.dataDropoutGeoChart.data;

    // const dataBubbleMap = props.dataDropoutBubbleMap;

    console.log(props.dataDropoutBubbleMap);

    const options = {
        chart: {
            map
        },
        title: {
            text: 'Peta Jumlah Mahasiswa Putus Studi di Indonesia'
        },
        subtitle: {
            text: "Demo of Highcharts map with bubbles"
          },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: "bottom"
            }
        },
        legend: {
            enabled: false
          },

        series: [
            {
              name: "Countries",
              color: "#E0E0E0",
              enableMouseTracking: false
            },
            {
              animation: {
                duration: 1000
              },
              type: "mapbubble",
              name: "Jumlah Mahasiswa Putus Studi",
              joinBy: ["hc-key", "code"],
              data: props.dataDropoutBubbleMap,
              minSize: 6,
              maxSize: "15%",
              tooltip: {
                pointFormat: "{point.properties.name}: {point.z} mahasiswa"
              }
            }
          ]
    }

    return (
        <div className="container mt-5">
            {/* <Box sx={{ minWidth: 550 }}>
                <FormControl sx={{ width: 550 }}>
                    <InputLabel id="demo-simple-select-label">Tahun/Semester</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={props.tahunSemester}
                        label="Tahun Ajaran"
                        onChange={props.handleDropdownChange}   
                    >
                        <MenuItem value={20171}>2017/1</MenuItem>
                        <MenuItem value={20172}>2017/2</MenuItem>
                        <MenuItem value={20161}>2016/1</MenuItem>
                    </Select>
                </FormControl>
            </Box> */}
            <HighchartsReact 
                highcharts={Highcharts}
                options={options}
                constructorType={'mapChart'}
            />
        </div>
    )


}

export default BubbleMap;