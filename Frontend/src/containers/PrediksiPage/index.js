import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { Button } from '@mui/material';
import axios from 'axios';
import { useState, useEffect } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useParams } from 'react-router';
import Link from '@mui/material/Link';
import Container from '@mui/material/Container';

function PrediksiPage () {

    const url = 'http://localhost:16200/api/'
    // const url = 'https://a7d4de21-2282-4e14-a7d4-4bfdea31d038.mock.pstmn.io/'
    const [searchedName, setSearchedName] = useState('');
    const [resultSearched, setResultSearched] = useState([]);

    const handleSearchNamaMahasiswa = async () => {
        console.log("handleSearchNamaMahasiswa kepanggil")
        await axios.get(`${url}nipd/${searchedName}`)
        .then ((response) => {
            setResultSearched(response.data)
        })
    }

    return (
        <Container fixed>
            <Box
            component="form"
            sx={{
                '& .MuiTextField-root': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off">
                <TextField
                    required
                    id="outlined-required"
                    type="search"
                    label="Nomor Induk Mahasiswa"
                    value={searchedName}
                    onChange={({target : {value}}) =>setSearchedName(value)}
                />
                <Button 
                    variant="contained"
                    onClick={handleSearchNamaMahasiswa}
                    value={searchedName}
                    size="small"
                    color="primary"
                    style={{ marginTop: `15px` }}>Cari</Button>
            </Box>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 1050 }} aria-label="simple table">
                    <TableHead>
                    <TableRow>
                        <TableCell><b>Nama Mahasiswa</b></TableCell>
                        <TableCell align="center"><b>NIM Mahasiswa</b></TableCell>
                        <TableCell align="center"><b>Jenis Kelamin</b></TableCell>
                        <TableCell align="center"><b>Nama Lembaga</b></TableCell>
                        <TableCell align="center"><b>Jurusan</b></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {resultSearched.map((row) => (
                        <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                        <TableCell component="th" scope="row">
                            <Link href={`/prediksi/${Object.values(row)[0]}`}>
                                {Object.values(row)[1]}
                            </Link>
                        </TableCell>
                        <TableCell align="center">
                            {Object.values(row)[2]}
                        </TableCell>
                        <TableCell align="center">{Object.values(row)[3]}</TableCell>
                        <TableCell align="center">{Object.values(row)[4]}</TableCell>
                        <TableCell align="center">{Object.values(row)[5]}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Container>
    )
}

export default PrediksiPage;