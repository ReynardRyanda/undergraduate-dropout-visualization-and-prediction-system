import React from 'react';
import { useState, useEffect } from 'react';
import SankeyChart from '../../components/SankeyDiagram';
import GeoChart from '../../components/GeoChart';
import BubbleMap from '../../components/BubbleMap';
import PieChart from '../../components/PieChart';
import BarChart from '../../components/BarChart';
import BarChartLulusDo from '../../components/BarChartLulusDO';
import axios from 'axios';

function App(){

    const url = 'http://localhost:16200/api/'
    // const url = 'https://19495645-5682-4880-a4a1-1fc5a505663f.mock.pstmn.io/'
    // const url = 'https://a7d4de21-2282-4e14-a7d4-4bfdea31d038.mock.pstmn.io/'
    const dataMap = {
        11:"id-ac",
        12:'id-su',
        13:'id-sb',
        16:'id-sl',
        14:'id-ri',
        21:'id-kr',
        15:'id-ja',
        17:'id-be',
        18:'id-1024',
        19:'id-bb',
        31:'id-jk',
        36:'id-bt',
        32:'id-jr',
        33:'id-jt',
        34:'id-yo',
        35:'id-ji',
        51:'id-ba',
        61:'id-kb',
        62:'id-kt',
        63:'id-ks',
        64:'id-ki',
        65:'id-ku',
        52:'id-nb',
        53:'id-nt',
        71:'id-sw',
        72:'id-st',
        73:'id-se',
        74:'id-sg',
        75:'id-go',
        76:'id-sr',
        81:'id-ma',
        82:'id-la',
        91:'id-pa',
        92:'id-ib'
    };

    // ============= ini Sankey diagram =====================
    
    const [tahunAjaran, setTahunAjaran] = useState('2015');
    const [dataDropoutFlow, setDataDropoutFlow] = useState('');
    const [isFetchingSankey, setIsFetchingSankey] = useState(true)
    
    useEffect(() => {
      getSankeyData()
    }, [tahunAjaran])

    const getSankeyData = async () => {
      await axios.get(`${url}sankey/${tahunAjaran}`)
      // await axios.get(`${url}rest/dropout/flow/${tahunAjaran}`)
      .then((response) =>{
        const sankeyDataApi = response.data.filter(item => Object.values(item)[0] > 0).map( item => [
          Object.keys(item)[0].substring(8, 14),
          Object.keys(item)[0].substring(15, 24),
          Object.values(item)[0]
        ])
        sankeyDataApi.unshift(["From","To","Number of students"])
        setDataDropoutFlow({data: sankeyDataApi});
      })
      .then((response) => {
          setTimeout(()=>{
              setIsFetchingSankey(false)
          }, 2000)
        })
      .catch(
        error => console.error(`Error: ${error}`)
      );
    }

    const handleTahunAjaranChange = (event) => {
      setTahunAjaran(event.target.value);
    };

    // ============= ini Geo Chart =====================

    const [tahunSemester, setTahunSemester] = useState('2015');
    
    const [dataJumlahDropoutGeoChart, setDataJumlahDropoutGeoChart] = useState('');
    const [dataDropoutGeoChart, setDataDropoutGeoChart] = useState('');

    useEffect(() => {
      getGeoChartData()
    }, [tahunSemester])

    // useEffect(() => {
    //   getGeoChartData()
    // }, [tahunSemester])

    const getGeoChartData = async () => {
      await axios.get(`${url}geochart/${tahunSemester}`)
      .then((response) =>{
        const geoChartDataApi = response.data.map(item => [
            dataMap[Object.values(item)[0]],
            parseFloat(((Object.values(item)[2] / (Object.values(item)[1] + Object.values(item)[2]))*100).toFixed(2))
        ]);
        // const geoChartDataApi = response.data.map(item => [
        //   dataMap[Object.values(item)[0]],
        //   Object.values(item)[2]
        // ]);
        // naruh data ke state
        setDataDropoutGeoChart({data: geoChartDataApi});
        // setDataJumlahDropoutGeoChart({data: geoChartPersentaseDataApi})
        }
      )
      .catch(
        error => console.error(`Error: ${error}`)
      );
    }

    const handleTahunSemesterChange = (event) => {
      setTahunSemester(event.target.value);
    };

    const handleJumlahChange = (event) => {
      setDataJumlahDropoutGeoChart(event.target.value);
    };

    // ============= ini Bubble Map =====================

    const [tahunSemesterBubbleMap, setTahunSemesterBubbleMap] = useState('20141');
    
    const [dataDropoutBubbleMap, setDataDropoutBubbleMap] = useState('');

    useEffect(() => {
      getBubbleMapData()
    }, [tahunSemester])

    const getBubbleMapData = async () => {
      await axios.get(`${url}geochart/${tahunSemester}`)
      .then((response) =>{
        const bubbleMapDataApi = response.data.map(item => {
          return {
              code: dataMap[Object.values(item)[0]],
              z: Object.values(item)[1]
          }
      });
        // naruh data ke state
        setDataDropoutBubbleMap(bubbleMapDataApi);
        }
      )
      .catch(
        error => console.error(`Error: ${error}`)
      );
    }

    const handleTahunSemesterChangeBubbleMap = (event) => {
      setTahunSemesterBubbleMap(event.target.value);
    };

    // ============= ini Pie Chart =====================

    const [tahunSemesterPieChart, setTahunSemesterPieChart] = useState('20141');
    
    const [dataDropoutPieChart, setDataDropoutPieChart] = useState('');
    
    useEffect(() => {
      getPieChartData()
    }, [tahunSemesterPieChart])

    const getPieChartData = async () => {
      await axios.get(`${url}status_mahasiswa/${tahunSemesterPieChart}`)
      .then((response) =>{
        const pieChartDataApi = response.data.map(item => [
            Object.values(response.data[response.data.indexOf(item)])[0],
            Object.values(response.data[response.data.indexOf(item)])[1]
        ])
        pieChartDataApi.unshift(["Status Mahasiswa","Number of students"])
        setDataDropoutPieChart(pieChartDataApi);
      })
      .catch(
        error => console.error(`Error: ${error}`)
      );
    }

    const handleTahunSemesterPieChartChange = (event) => {
      setTahunSemesterPieChart(event.target.value);
    };

    // ============= ini Bar Chart =====================

    
    const [tahunBarChart, setTahunBarChart] = useState('2015');
    const [dataDropoutBarChart, setDataDropoutBarChart] = useState('')
    
    
    useEffect(() => {
      getBarChartData()
    }, [tahunBarChart])

    const getBarChartData = async () => {
      await axios.get(`${url}do_per_semester/${tahunBarChart}`)
      .then((response) =>{
        const barChartDataApi = response.data.map(item => [
          Object.keys(response.data[response.data.indexOf(item)])[0].substring(9,15),
          Object.values(response.data[response.data.indexOf(item)])[0]
        ])
        barChartDataApi.unshift(["Semester","Jumlah Drop Out"])
        setDataDropoutBarChart({data: barChartDataApi});
      })
      .catch(
        error => console.error(`Error: ${error}`)
      );
    }

    const handleTahunBarChartChange = (event) => {
      setTahunBarChart(event.target.value);
    };

    // ============= ini Bar Chart lulus dan DO =====================


    // const [tahunBarChart, setTahunBarChart] = useState('2015');
    const [dataLulusDropoutBarChart, setDataLulusDropoutBarChart] = useState('')
    
    useEffect(() => {
      getBarChartLulusDoData()
    }, [])

    const getBarChartLulusDoData = async () => {
      await axios.get(`${url}barchart`)
      .then((response) =>{
        const barChartLulusDoDataApi = response.data.map(item => [
          Object.values(response.data[response.data.indexOf(item)])[0].substring(0,4),
          Object.values(response.data[response.data.indexOf(item)])[1],
          Object.values(response.data[response.data.indexOf(item)])[2]
        ])
        console.log(barChartLulusDoDataApi)
        console.log(barChartLulusDoDataApi)
        barChartLulusDoDataApi.unshift(["Tahun Angkatan","Jumlah Mahasiswa Lulus", "Jumlah Mahasiswa Drop out"])
        setDataLulusDropoutBarChart({data: barChartLulusDoDataApi});
        console.log(dataLulusDropoutBarChart)
        console.log(dataLulusDropoutBarChart)
      })
      .catch(
        error => console.error(`Error: ${error}`)
      );
    }


    return(
        <div className="App">
            <SankeyChart  
              dataDropoutFlow={dataDropoutFlow}
              isFetchingSankey={isFetchingSankey}
              handleDropdownChange={handleTahunAjaranChange}/>
            <GeoChart 
              dataDropoutGeoChart={dataDropoutGeoChart}
              handleDropdownChange={handleTahunSemesterChange}
              handleButtonChange={handleJumlahChange}/>
            {/* <BubbleMap 
              dataDropoutBubbleMap={dataDropoutBubbleMap}
              handleTahunSemesterChangeBubbleMap={handleTahunSemesterChangeBubbleMap}
            /> */}
            {/* <PieChart 
              dataDropoutPieChart={dataDropoutPieChart}
              handleTahunSemesterPieChartChange = {handleTahunSemesterPieChartChange}
            /> */}
            <BarChart  
              dataDropoutBarChart={dataDropoutBarChart}
              handleTahunBarChartChange={handleTahunBarChartChange}/>
            <BarChartLulusDo  
            dataLulusDropoutBarChart={dataLulusDropoutBarChart}/>
        </div>
    )     
}
export default App;
