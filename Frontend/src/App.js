// import React, { useEffect, useState } from 'react';
import ReactDOM from "react-dom/client";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import './App.css';
import App from './containers/App';
import VisualisasiPage from './containers/VisualisasiPage';
import PrediksiPage from './containers/PrediksiPage';
import ModellingPage from './containers/ModellingPage';
import Navbar from '../src/components/Navbar';
import DetailMahasiswa from '../src/components/DetailMahasiswa'

function render() {

    const root = ReactDOM.createRoot(
        document.getElementById("root")
      );
      root.render(
        <BrowserRouter>
        <Navbar></Navbar>
          <Routes>
            <Route exact path="/" element={<App />}> </Route>
            <Route exact path="/visualisasi" element={<VisualisasiPage />}> </Route>
            <Route exact path="/prediksi" element={<PrediksiPage />}></Route>
            <Route exact path="/prediksi/:id_reg" element={<DetailMahasiswa />}> </Route>
            <Route exact path="/modelling" element={<ModellingPage />}></Route>
          </Routes>
        </BrowserRouter>
      );

}

export default render;
