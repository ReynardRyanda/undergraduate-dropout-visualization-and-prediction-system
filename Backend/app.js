var Db  = require('./dboperations');
const dboperations = require('./dboperations');

var express = require('express');
var bodyParser = require('body-parser');
// var fetch = require('node-fetch');
var cors = require('cors');
var app = express();
var router = express.Router();
var expressStatusMonitor = require('express-status-monitor');
var process = require('process')
var os 	= require('os-utils');
// let redisClient;

// (async () => {
//   redisClient = redis.createClient();

//   redisClient.on("error", (error) => console.error(`Error : ${error}`));

//   await redisClient.connect();
// })();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(fetch());
// app.use(axios());
app.use(cors());
app.use(expressStatusMonitor());
app.use('/api', router);

// router.use((request,response,next)=>{
//     console.log('middleware');
//     next();
// })

router.route('/sankey/:year').get((request,response)=>{
    dboperations.getSankey(request.params.year).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/sankey/:year) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/sankey/:year): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/sankey/:year): ' + v );
    });
})
router.route('/sankey/:year/nonindex').get((request,response)=>{
    dboperations.getSankeyNonIndex(request.params.year).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/sankey/:year) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/sankey/:year): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/sankey/:year): ' + v );
    });
})

router.route('/geochart/:year').get((request,response)=>{
    dboperations.getGeo(request.params.year).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/geochart/:year) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/geochart/:year): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/geochart/:year): ' + v );
    });
})

router.route('/geochart/:year/nonindex').get((request,response)=>{
    dboperations.getGeoNonIndex(request.params.year).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/geochart/:year) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/geochart/:year): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/geochart/:year): ' + v );
    });
})

router.route('/barchart').get((request,response)=>{
    dboperations.getBarchart().then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/barchart) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/barchart): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/barchart): ' + v );
    });
})

router.route('/barchart/nonindex').get((request,response)=>{
    dboperations.getBarchartNonIndex().then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/barchart) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/barchart): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/barchart): ' + v );
    });
})

router.route('/do_per_semester/:year').get((request,response)=>{
    dboperations.getDOPerSemester(request.params.year).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/do_per_semester/:year) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/do_per_semester/:year): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/do_per_semester/:year): ' + v );
    });
})

router.route('/do_per_semester/:year/nonindex').get((request,response)=>{
    dboperations.getDOPerSemesterNonIndex(request.params.year).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/do_per_semester/:year) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/do_per_semester/:year): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/do_per_semester/:year): ' + v );
    });
})

router.route('/nama/:name').get((request,response)=>{
    dboperations.getName(request.params.name).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/nama/:name) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/nama/:name): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/nama/:name): ' + v );
    });
})

router.route('/nipd/:nipd').get((request,response)=>{
    dboperations.getNipd(request.params.nipd).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/nipd/:nipd) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/nipd/:nipd): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/nipd/:nipd): ' + v );
    });
})

router.route('/nipd/:nipd/nonindex').get((request,response)=>{
    dboperations.getNipdNonIndex(request.params.nipd).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/nipd/:nipd) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/nipd/:nipd): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/nipd/:nipd): ' + v );
    });
})

router.route('/id_reg_pd/:idRegPd').get((request,response)=>{
    dboperations.getIdRegPd(request.params.idRegPd).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/id_reg_pd/:idRegPd) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/id_reg_pd/:idRegPd): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/do_per_semester/:idRegPd): ' + v );
    });
})

router.route('/id_reg_pd/:idRegPd/nonindex').get((request,response)=>{
    dboperations.getIdRegPdNonIndex(request.params.idRegPd).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/id_reg_pd/:idRegPd) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/id_reg_pd/:idRegPd): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/do_per_semester/:idRegPd): ' + v );
    });
})

router.route('/predict').post((request,response)=>{
    dboperations.postPredict(request.body).then(result => {
        response.json(result);
    })
    for (const [key,value] of Object.entries(process.memoryUsage())){
        console.log(`Memory usage (/predict) by ${key}, ${value/1000000}MB `)
    }
    os.cpuUsage(function(v){
        console.log( 'CPU Usage (%) (/predict): ' + v );
    });
    
    os.cpuFree(function(v){
        console.log( 'CPU Free (%) (/predict): ' + v );
    });
})

// router.route('/status_mahasiswa/:semester').get((request,response)=>{
//     dboperations.getStatusMahasiswa(request.params.semester).then(result => {
//         response.json(result);
//     })
//     for (const [key,value] of Object.entries(process.memoryUsage())){
//         console.log(`Memory usage (/status_mahasiswa/:semester) by ${key}, ${value/1000000}MB `)
//     }
//     os.cpuUsage(function(v){
//         console.log( 'CPU Usage (%) (/status_mahasiswa/:semester): ' + v );
//     });
    
//     os.cpuFree(function(v){
//         console.log( 'CPU Free (%) (/status_mahasiswa/:semester): ' + v );
//     });
// })

// router.route('/jenis_keluar/:semester').get((request,response)=>{
//     dboperations.getJenisKeluar(request.params.semester).then(result => {
//         response.json(result);
//     })
//     for (const [key,value] of Object.entries(process.memoryUsage())){
//         console.log(`Memory usage (/jenis_keluar/:semester) by ${key}, ${value/1000000}MB `)
//     }
//     os.cpuUsage(function(v){
//         console.log( 'CPU Usage (%) (/jenis_keluar/:semester): ' + v );
//     });
    
//     os.cpuFree(function(v){
//         console.log( 'CPU Free (%) (/jenis_keluar/:semester): ' + v );
//     });
// })

// router.route('/ipk/:semester').get((request,response)=>{
//     dboperations.getIpk(request.params.semester).then(result => {
//         response.json(result);
//     })
// })

var port = process.env.PORT || 8090;
app.listen(port);
console.log('PDDikti API is runnning at ' + port);