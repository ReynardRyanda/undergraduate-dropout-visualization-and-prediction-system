const dotenv = require('dotenv');
dotenv.config();

const config = {
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    server: process.env.DATABASE_SERVER,
    database: process.env.DATABASE_NAME,
    options:{
        trustedconnection: true,
        enableArithAbort : true,
        trustServerCertificate: true
    },
    port : parseInt(process.env.DATABASE_PORT, 10),
    requestTimeout: 300000
}

module.exports = config;