var config = require('./dbconfig');
const sql = require('mssql');
const axios = require('axios');
const { query } = require('express');
const crypto = require('crypto');

async function getSankey(year) {
    try {
        console.time('GET api/sankey/:year');
        let query = ""
        for (let yearCount = 0; yearCount < 7; yearCount++) {
            for (let semesterCount = 0; semesterCount < 2; semesterCount++) {
                query += `
                SELECT id_reg_pd
                INTO #aktif_smt_${yearCount}${semesterCount}
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount}${1 + semesterCount}' AND id_stat_mhs = 'A';

                SELECT id_reg_pd
                INTO #pasif_smt_${yearCount}${semesterCount}
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount}${1 + semesterCount}' AND id_stat_mhs IN ('C', 'N');               

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}A
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs = 'A' AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}P
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs IN ('C', 'N') AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_DO
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar IN ('3', '4', '5') AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_Graduated
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar = '1' AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}A
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs = 'A' AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}P
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs IN ('C', 'N') AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_DO
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar IN ('3', '4', '5') AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_Graduated
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar = '1' AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});
                `
            }
        }
        console.log(query);
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('year', sql.Int, year)
        .query(query);
        let result = []
        for (let i = 0; i < agregat.recordsets.length; i++) {
            result.push(agregat.recordsets[i][0]);
        }
        console.timeEnd('GET api/sankey/:year');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

async function getSankeyNonIndex(year) {
    try {
        console.time('GET api/sankey/:year');
        let query = ""
        for (let yearCount = 0; yearCount < 7; yearCount++) {
            for (let semesterCount = 0; semesterCount < 2; semesterCount++) {
                query += `
                SELECT id_reg_pd
                INTO #aktif_smt_${yearCount}${semesterCount}
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount}${1 + semesterCount}' AND id_stat_mhs = 'A';

                SELECT id_reg_pd
                INTO #pasif_smt_${yearCount}${semesterCount}
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount}${1 + semesterCount}' AND id_stat_mhs IN ('C', 'N');               

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}A
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs = 'A' AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}P
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs IN ('C', 'N') AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_DO
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar IN ('3', '4', '5') AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}A_Graduated
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar = '1' AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #aktif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}A
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs = 'A' AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}P
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_stat_mhs IN ('C', 'N') AND row_rank != 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_DO
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar IN ('3', '4', '5') AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});

                SELECT COUNT(id_reg_pd) AS agregat_${parseInt(year) + yearCount}${1 + semesterCount}P_Graduated
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount + semesterCount}${2 - semesterCount}' AND id_jns_keluar = '1' AND row_rank = 1 AND id_reg_pd IN (SELECT * FROM #pasif_smt_${yearCount}${semesterCount});
                `
            }
        }
        console.log(query);
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('year', sql.Int, year)
        .query(query);
        let result = []
        for (let i = 0; i < agregat.recordsets.length; i++) {
            result.push(agregat.recordsets[i][0]);
        }
        console.timeEnd('GET api/sankey/:year');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

async function getGeo(year) {
    try {
        console.time('GET api/geochart/:year');
        let query = `
        SELECT SUBSTRING (nik, 1, 2) AS kode_wilayah, COUNT(*) AS total_lulus
        FROM agregat_visualisasi4_index
        WHERE mulai_smt = '${parseInt(year)}1' AND id_jns_keluar = '1'  
        GROUP BY SUBSTRING (nik, 1, 2);
        SELECT SUBSTRING (nik, 1, 2) AS kode_wilayah, COUNT(*) AS total_DO
        FROM agregat_visualisasi4_index
        WHERE mulai_smt = '${parseInt(year)}1' AND id_jns_keluar IN ('3', '4', '5') 
        GROUP BY SUBSTRING (nik, 1, 2);
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('year', sql.Int, year)
        .query(query);
        let kodeWilayah = {
            '11':'Aceh',
            '12':'Sumatera Utara',
            '13':'Sumatera Barat',
            '16':'Sumatera Selatan',
            '14':'Riau',
            '21':'Kepulauan Riau',
            '15':'Jambi',
            '17':'Bengkulu',
            '18':'Lampung',
            '19':'Bangka Belitung',
            '31':'DKI Jakarta',
            '36':'Banten',
            '32':'Jawa Barat',
            '33':'Jawa Tengah',
            '34':'DI Yogyakarta',
            '35':'Jawa Timur',
            '51':'Bali',
            '61':'Kalimantan Barat',
            '62':'Kalimantan Tengah',
            '63':'Kalimantan Selatan',
            '64':'Kalimantan Timur',
            '65':'Kalimantan Utara',
            '52':'Nusa Tenggara Barat',
            '53':'Nusa Tenggara Timur',
            '71':'Sulawesi Utara',
            '72':'Sulawesi Tengah',
            '73':'Sulawesi Selatan',
            '74':'Sulawesi Tenggara',
            '75':'Gorontalo',
            '76':'Sulawesi Barat',
            '81':'Maluku',
            '82':'Maluku Utara',
            '91':'Papua',
            '92':'Papua Barat'
        }
        let result = [];
        let data = [...agregat.recordsets[0], ...agregat.recordsets[1]];
        let allData = data.reduce((res, element) => {
            res[element['kode_wilayah']] = {
                ...res[element['kode_wilayah']],
                ...element
            }
            return res
        }, {});
        Object.values(allData).forEach(value => {
            if (Object.values(value)[0] in kodeWilayah) {
                result.push(value);
            }
        })
        console.timeEnd('GET api/geochart/:year');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

async function getGeoNonIndex(year) {
    try {
        console.time('GET api/geochart/:year');
        let query = `
        SELECT SUBSTRING (nik, 1, 2) AS kode_wilayah, COUNT(*) AS total_lulus
        FROM agregat_visualisasi5
        WHERE mulai_smt = '${parseInt(year)}1' AND id_jns_keluar = '1'  
        GROUP BY SUBSTRING (nik, 1, 2);
        SELECT SUBSTRING (nik, 1, 2) AS kode_wilayah, COUNT(*) AS total_DO
        FROM agregat_visualisasi5
        WHERE mulai_smt = '${parseInt(year)}1' AND id_jns_keluar IN ('3', '4', '5') 
        GROUP BY SUBSTRING (nik, 1, 2);
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('year', sql.Int, year)
        .query(query);
        let kodeWilayah = {
            '11':'Aceh',
            '12':'Sumatera Utara',
            '13':'Sumatera Barat',
            '16':'Sumatera Selatan',
            '14':'Riau',
            '21':'Kepulauan Riau',
            '15':'Jambi',
            '17':'Bengkulu',
            '18':'Lampung',
            '19':'Bangka Belitung',
            '31':'DKI Jakarta',
            '36':'Banten',
            '32':'Jawa Barat',
            '33':'Jawa Tengah',
            '34':'DI Yogyakarta',
            '35':'Jawa Timur',
            '51':'Bali',
            '61':'Kalimantan Barat',
            '62':'Kalimantan Tengah',
            '63':'Kalimantan Selatan',
            '64':'Kalimantan Timur',
            '65':'Kalimantan Utara',
            '52':'Nusa Tenggara Barat',
            '53':'Nusa Tenggara Timur',
            '71':'Sulawesi Utara',
            '72':'Sulawesi Tengah',
            '73':'Sulawesi Selatan',
            '74':'Sulawesi Tenggara',
            '75':'Gorontalo',
            '76':'Sulawesi Barat',
            '81':'Maluku',
            '82':'Maluku Utara',
            '91':'Papua',
            '92':'Papua Barat'
        }
        let result = [];
        let data = [...agregat.recordsets[0], ...agregat.recordsets[1]];
        let allData = data.reduce((res, element) => {
            res[element['kode_wilayah']] = {
                ...res[element['kode_wilayah']],
                ...element
            }
            return res
        }, {});
        Object.values(allData).forEach(value => {
            if (Object.values(value)[0] in kodeWilayah) {
                result.push(value);
            }
        })
        console.timeEnd('GET api/geochart/:year');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

async function getBarchart() {
    try {
        console.time('GET api/barchart');
        let query = `
        SELECT mulai_smt, COUNT(*) AS total_lulus
        FROM agregat_visualisasi4_index
        WHERE id_jns_keluar IN ('1') AND row_rank = 1
        GROUP BY mulai_smt;
        SELECT mulai_smt, COUNT(*) AS total_DO
        FROM agregat_visualisasi4_index
        WHERE id_jns_keluar IN ('3', '4', '5') AND row_rank = 1
        GROUP BY mulai_smt;
        `
        let pool = await sql.connect(config);
        let showplan = await pool.request().query(`SET SHOWPLAN_ALL ON`);
        let agregat = await pool.request()
        .query(query);
        let result = [];
        console.log(agregat);
        let data = [...agregat.recordsets[0], ...agregat.recordsets[1]];
        let allData = data.reduce((res, element) => {
            res[element['mulai_smt']] = {
                ...res[element['mulai_smt']],
                ...element
            }
            return res
        }, {});
        Object.values(allData).forEach(value => {
            result.push(value);
        })
        console.timeEnd('GET api/barchart');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

async function getBarchartNonIndex() {
    try {
        console.time('GET api/barchart');
        let query = `
        SELECT mulai_smt, COUNT(*) AS total_lulus
        FROM agregat_visualisasi5
        WHERE id_jns_keluar IN ('1') AND row_rank = 1
        GROUP BY mulai_smt;
        SELECT mulai_smt, COUNT(*) AS total_DO
        FROM agregat_visualisasi5
        WHERE id_jns_keluar IN ('3', '4', '5') AND row_rank = 1
        GROUP BY mulai_smt;
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .query(query);
        let result = [];
        let data = [...agregat.recordsets[0], ...agregat.recordsets[1]];
        let allData = data.reduce((res, element) => {
            res[element['mulai_smt']] = {
                ...res[element['mulai_smt']],
                ...element
            }
            return res
        }, {});
        Object.values(allData).forEach(value => {
            result.push(value);
        })
        console.timeEnd('GET api/barchart');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

async function getName(name) {
    try {
        console.time('GET api/nama');
        let query = `
        SELECT TOP 20 *
        FROM agregat_mahasiswa3
        WHERE LOWER(nm_pd) LIKE '%${name}%';
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('name', sql.VarChar, name)
        .query(query);
        console.log(agregat.recordsets);
        console.timeEnd('GET api/nama');
        return agregat.recordsets[0];
    }
    catch (error) {
        console.log(error);
    }
}

async function getNipd(nipd) {
    try {
        console.time('GET nipd/:nipd');
        let query = `
        SELECT TOP 20 *
        FROM agregat_mahasiswa3_index
        WHERE nipd LIKE '%${nipd}%';
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('nipd', sql.Int, nipd)
        .query(query);
        console.log(agregat.recordset);
        agregat.recordset.forEach(element => {
            element['nm_pd'] = crypto.createHash('md5').update(element['nm_pd']).digest('hex');
            element['nipd'] = crypto.createHash('md5').update(element['nipd'].substring(0, element['nipd'].length - 5)).digest('hex') + element['nipd'].substring(element['nipd'].length - 5);
            element['nm_lemb_sp'] = crypto.createHash('md5').update(element['nm_lemb_sp']).digest('hex');
            element['nm_lemb_sms'] = crypto.createHash('md5').update(element['nm_lemb_sms']).digest('hex');
        });
        console.timeEnd('GET nipd/:nipd');
        return agregat.recordset;
    }
    catch (error) {
        console.log(error);
    }
}

async function getNipdNonIndex(nipd) {
    try {
        console.time('GET nipd/:nipd');
        let query = `
        SELECT TOP 20 *
        FROM agregat_mahasiswa3
        WHERE nipd LIKE '%${nipd}%';
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('nipd', sql.Int, nipd)
        .query(query);
        console.log(agregat.recordset);
        agregat.recordset.forEach(element => {
            element['nm_pd'] = crypto.createHash('md5').update(element['nm_pd']).digest('hex');
            element['nipd'] = crypto.createHash('md5').update(element['nipd'].substring(0, element['nipd'].length - 5)).digest('hex') + element['nipd'].substring(element['nipd'].length - 5);
            element['nm_lemb_sp'] = crypto.createHash('md5').update(element['nm_lemb_sp']).digest('hex');
            element['nm_lemb_sms'] = crypto.createHash('md5').update(element['nm_lemb_sms']).digest('hex');
        });
        console.timeEnd('GET nipd/:nipd');
        return agregat.recordset;
    }
    catch (error) {
        console.log(error);
    }
}

async function getIdRegPd(idRegPd) {
    try {
        console.time('GET api/id_reg_pd');
        let query = `
        SELECT *
        FROM agregat_prediksi5_index
        WHERE id_reg_pd = '${idRegPd}' AND semester_rank IN ('1', '2')
        ORDER BY id_smt ASC;
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('idRegPd', sql.VarChar, idRegPd)
        .query(query);
        console.log(agregat.recordset);
        let merantau = 0;
        temp = {};
        temp['nm_pd'] = crypto.createHash('md5').update(agregat.recordsets[0][0]['nm_pd']).digest('hex');
        temp['nipd'] = crypto.createHash('md5').update(agregat.recordsets[0][0]['nipd'].substring(0, agregat.recordsets[0][0]['nipd'].length - 5)).digest('hex') + agregat.recordsets[0][0]['nipd'].substring(agregat.recordsets[0][0]['nipd'].length - 5);
        temp['agama'] = agregat.recordsets[0][0]['id_agama'];
        temp['jk'] = agregat.recordsets[0][0]['jk'] == 'L' ? 1 : 0;
        temp['tgl_lahir'] = agregat.recordsets[0][0]['tgl_lahir'];
        temp['tgl_masuk_sp'] = agregat.recordsets[0][0]['tgl_masuk_sp'];
        temp['sp'] = crypto.createHash('md5').update(agregat.recordsets[0][0]['nm_lemb']).digest('hex');
        nik = agregat.recordsets[0][0]['nik'];
        id_wil_sp = agregat.recordsets[0][0]['id_wil_sp'];
        let kode_wilayah_nik = {
            '11':'Aceh',
            '12':'Sumatera Utara',
            '13':'Sumatera Barat',
            '16':'Sumatera Selatan',
            '14':'Riau',
            '21':'Kepulauan Riau',
            '15':'Jambi',
            '17':'Bengkulu',
            '18':'Lampung',
            '19':'Bangka Belitung',
            '31':'DKI Jakarta',
            '36':'Banten',
            '32':'Jawa Barat',
            '33':'Jawa Tengah',
            '34':'DI Yogyakarta',
            '35':'Jawa Timur',
            '51':'Bali',
            '61':'Kalimantan Barat',
            '62':'Kalimantan Tengah',
            '63':'Kalimantan Selatan',
            '64':'Kalimantan Timur',
            '65':'Kalimantan Utara',
            '52':'Nusa Tenggara Barat',
            '53':'Nusa Tenggara Timur',
            '71':'Sulawesi Utara',
            '72':'Sulawesi Tengah',
            '73':'Sulawesi Selatan',
            '74':'Sulawesi Tenggara',
            '75':'Gorontalo',
            '76':'Sulawesi Barat',
            '81':'Maluku',
            '82':'Maluku Utara',
            '91':'Papua',
            '92':'Papua Barat'
        }
        let kode_wilayah_pddikti = {
            '06':'Aceh',
            '07':'Sumatera Utara',
            '08':'Sumatera Barat',
            '11':'Sumatera Selatan',
            '09':'Riau',
            '31':'Kepulauan Riau',
            '10':'Jambi',
            '26':'Bengkulu',
            '12':'Lampung',
            '29':'Bangka Belitung',
            '01':'DKI Jakarta',
            '28':'Banten',
            '02':'Jawa Barat',
            '03':'Jawa Tengah',
            '04':'DI Yogyakarta',
            '05':'Jawa Timur',
            '22':'Bali',
            '13':'Kalimantan Barat',
            '14':'Kalimantan Tengah',
            '15':'Kalimantan Selatan',
            '16':'Kalimantan Timur',
            '34':'Kalimantan Utara',
            '23':'Nusa Tenggara Barat',
            '24':'Nusa Tenggara Timur',
            '17':'Sulawesi Utara',
            '18':'Sulawesi Tengah',
            '19':'Sulawesi Selatan',
            '20':'Sulawesi Tenggara',
            '30':'Gorontalo',
            '33':'Sulawesi Barat',
            '21':'Maluku',
            '27':'Maluku Utara',
            '25':'Papua',
            '32':'Papua Barat'
        }
        if (nik.substring(0, 2) in kode_wilayah_nik && id_wil_sp.substring(0, 2) in kode_wilayah_pddikti) {
            if (kode_wilayah_nik[nik.substring(0, 2)] != kode_wilayah_pddikti[id_wil_sp.substring(0, 2)])  {
                merantau = 1;
            } 
        }
        temp['merantau'] = merantau;
        temp['sks_smt_1'] = agregat.recordsets[0][0]['sks_smt'];
        temp['sks_total_1'] = agregat.recordsets[0][0]['sks_total'];
        temp['ips_1'] = agregat.recordsets[0][0]['ips'];
        temp['ipk_1'] = agregat.recordsets[0][0]['ipk'];
        sem1filter = agregat.recordsets[0].filter(item => item['semester_rank'] == 1)
        sem1 = sem1filter.map(item => {
            return {
                'nm_mk': item['nm_mk'],
                'nilai_indeks': item['nilai_indeks']
            }
        });
        temp['kelas_smt_1'] = sem1;
        temp['sks_smt_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['sks_smt'];
        temp['sks_total_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['sks_total'];
        temp['ips_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['ips'];
        temp['ipk_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['ipk'];
        sem2 = agregat.recordsets[0].filter(item => item['semester_rank'] == 2).map(item => {
            return {
                'nm_mk': item['nm_mk'],
                'nilai_indeks': item['nilai_indeks']
            }
        });
        temp['kelas_smt_2'] = sem2;
        console.timeEnd('GET api/id_reg_pd');
        return temp;
    }
    catch (error) {
        console.log(error);
    }
}

async function getIdRegPdNonIndex(idRegPd) {
    try {
        console.time('GET api/id_reg_pd');
        let query = `
        SELECT *
        FROM agregat_prediksi5
        WHERE id_reg_pd = '${idRegPd}' AND semester_rank IN ('1', '2')
        ORDER BY id_smt ASC;
        `
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('idRegPd', sql.VarChar, idRegPd)
        .query(query);
        console.log(agregat.recordset);
        let merantau = 0;
        temp = {};
        temp['nm_pd'] = crypto.createHash('md5').update(agregat.recordsets[0][0]['nm_pd']).digest('hex');
        temp['nipd'] = crypto.createHash('md5').update(agregat.recordsets[0][0]['nipd'].substring(0, agregat.recordsets[0][0]['nipd'].length - 5)).digest('hex') + agregat.recordsets[0][0]['nipd'].substring(agregat.recordsets[0][0]['nipd'].length - 5);
        temp['agama'] = agregat.recordsets[0][0]['id_agama'];
        temp['jk'] = agregat.recordsets[0][0]['jk'] == 'L' ? 1 : 0;
        temp['tgl_lahir'] = agregat.recordsets[0][0]['tgl_lahir'];
        temp['tgl_masuk_sp'] = agregat.recordsets[0][0]['tgl_masuk_sp'];
        temp['sp'] = crypto.createHash('md5').update(agregat.recordsets[0][0]['nm_lemb']).digest('hex');
        nik = agregat.recordsets[0][0]['nik'];
        id_wil_sp = agregat.recordsets[0][0]['id_wil_sp'];
        let kode_wilayah_nik = {
            '11':'Aceh',
            '12':'Sumatera Utara',
            '13':'Sumatera Barat',
            '16':'Sumatera Selatan',
            '14':'Riau',
            '21':'Kepulauan Riau',
            '15':'Jambi',
            '17':'Bengkulu',
            '18':'Lampung',
            '19':'Bangka Belitung',
            '31':'DKI Jakarta',
            '36':'Banten',
            '32':'Jawa Barat',
            '33':'Jawa Tengah',
            '34':'DI Yogyakarta',
            '35':'Jawa Timur',
            '51':'Bali',
            '61':'Kalimantan Barat',
            '62':'Kalimantan Tengah',
            '63':'Kalimantan Selatan',
            '64':'Kalimantan Timur',
            '65':'Kalimantan Utara',
            '52':'Nusa Tenggara Barat',
            '53':'Nusa Tenggara Timur',
            '71':'Sulawesi Utara',
            '72':'Sulawesi Tengah',
            '73':'Sulawesi Selatan',
            '74':'Sulawesi Tenggara',
            '75':'Gorontalo',
            '76':'Sulawesi Barat',
            '81':'Maluku',
            '82':'Maluku Utara',
            '91':'Papua',
            '92':'Papua Barat'
        }
        let kode_wilayah_pddikti = {
            '06':'Aceh',
            '07':'Sumatera Utara',
            '08':'Sumatera Barat',
            '11':'Sumatera Selatan',
            '09':'Riau',
            '31':'Kepulauan Riau',
            '10':'Jambi',
            '26':'Bengkulu',
            '12':'Lampung',
            '29':'Bangka Belitung',
            '01':'DKI Jakarta',
            '28':'Banten',
            '02':'Jawa Barat',
            '03':'Jawa Tengah',
            '04':'DI Yogyakarta',
            '05':'Jawa Timur',
            '22':'Bali',
            '13':'Kalimantan Barat',
            '14':'Kalimantan Tengah',
            '15':'Kalimantan Selatan',
            '16':'Kalimantan Timur',
            '34':'Kalimantan Utara',
            '23':'Nusa Tenggara Barat',
            '24':'Nusa Tenggara Timur',
            '17':'Sulawesi Utara',
            '18':'Sulawesi Tengah',
            '19':'Sulawesi Selatan',
            '20':'Sulawesi Tenggara',
            '30':'Gorontalo',
            '33':'Sulawesi Barat',
            '21':'Maluku',
            '27':'Maluku Utara',
            '25':'Papua',
            '32':'Papua Barat'
        }
        if (nik.substring(0, 2) in kode_wilayah_nik && id_wil_sp.substring(0, 2) in kode_wilayah_pddikti) {
            if (kode_wilayah_nik[nik.substring(0, 2)] != kode_wilayah_pddikti[id_wil_sp.substring(0, 2)])  {
                merantau = 1;
            } 
        }
        temp['merantau'] = merantau;
        temp['sks_smt_1'] = agregat.recordsets[0][0]['sks_smt'];
        temp['sks_total_1'] = agregat.recordsets[0][0]['sks_total'];
        temp['ips_1'] = agregat.recordsets[0][0]['ips'];
        temp['ipk_1'] = agregat.recordsets[0][0]['ipk'];
        sem1filter = agregat.recordsets[0].filter(item => item['semester_rank'] == 1)
        sem1 = sem1filter.map(item => {
            return {
                'nm_mk': item['nm_mk'],
                'nilai_indeks': item['nilai_indeks']
            }
        });
        temp['kelas_smt_1'] = sem1;
        temp['sks_smt_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['sks_smt'];
        temp['sks_total_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['sks_total'];
        temp['ips_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['ips'];
        temp['ipk_2'] = agregat.recordsets[0][agregat.recordsets[0].length - 1]['ipk'];
        sem2 = agregat.recordsets[0].filter(item => item['semester_rank'] == 2).map(item => {
            return {
                'nm_mk': item['nm_mk'],
                'nilai_indeks': item['nilai_indeks']
            }
        });
        temp['kelas_smt_2'] = sem2;
        console.timeEnd('GET api/id_reg_pd');
        return temp;
    }
    catch (error) {
        console.log(error);
    }
}

async function getDOPerSemester(year) {
    try {
        console.time('GET api/do_per_semester');
        let query = ""
        for (let yearCount = 0; yearCount < 7; yearCount++) {
            for (let semesterCount = 0; semesterCount < 2; semesterCount++) {
                query += `
                SELECT COUNT(*) AS total_DO_smt_${yearCount * 2 + 1 + semesterCount}
                FROM agregat_visualisasi4_index
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount}${1 + semesterCount}' AND id_jns_keluar IN ('3', '4', '5') AND row_rank = 1;
                `
            }
        }
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('year', sql.Int, year)
        .query(query);
        let result = [];
        for (let i = 0; i < agregat.recordsets.length; i++) {
            result.push(agregat.recordsets[i][0]);
        }
        console.timeEnd('GET api/do_per_semester');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

async function getDOPerSemesterNonIndex(year) {
    try {
        console.time('GET api/do_per_semester');
        let query = ""
        for (let yearCount = 0; yearCount < 7; yearCount++) {
            for (let semesterCount = 0; semesterCount < 2; semesterCount++) {
                query += `
                SELECT COUNT(*) AS total_DO_smt_${yearCount * 2 + 1 + semesterCount}
                FROM agregat_visualisasi5
                WHERE mulai_smt = '${parseInt(year)}1' AND id_smt = '${parseInt(year) + yearCount}${1 + semesterCount}' AND id_jns_keluar IN ('3', '4', '5') AND row_rank = 1;
                `
            }
        }
        let pool = await sql.connect(config);
        let agregat = await pool.request()
        .input('year', sql.Int, year)
        .query(query);
        let result = [];
        for (let i = 0; i < agregat.recordsets.length; i++) {
            result.push(agregat.recordsets[i][0]);
        }
        console.timeEnd('GET api/do_per_semester');
        return result;
    }
    catch (error) {
        console.log(error);
    }
}

function standardDeviation(arr){
    let mean = arr.reduce((acc, curr)=>{
        return acc + curr
    }, 0) / arr.length;

    arr = arr.map((el)=>{
        return (el - mean) ** 2
    })

    let total = arr.reduce((acc, curr)=> acc + curr, 0);

    return Math.sqrt(total / arr.length)
}

function dateDiffInYears(dateold, datenew) {
    var ynew = datenew.substring(0, 4);
    var mnew = datenew.substring(5, 7);
    var dnew = datenew.substring(8);
    var yold = dateold.substring(0, 4);
    var mold = dateold.substring(5, 7);
    var dold = dateold.substring(8);
    var diff = ynew - yold;
    if (mold > mnew) diff--;
    else {
        if (mold == mnew) {
            if (dold > dnew) diff--;
        }
    }
    return diff;
}

async function postPredict(body) {
    try {
        let kelas_smt_1 = body['kelas_smt_1'];
        let kelas_smt_2 = body['kelas_smt_2'];
        let tgl_masuk_sp = body ['tgl_masuk_sp'].split("T")[0];
        let tgl_lahir = body ['tgl_lahir'].split("T")[0];
        let umur_saat_masuk = dateDiffInYears(tgl_masuk_sp, tgl_lahir);
        let nilai_tertinggi_1 = Math.max(...kelas_smt_1);
        let nilai_terendah_1 = Math.min(...kelas_smt_1);
        const median = arr => {
            const mid = Math.floor(arr.length / 2),
              nums = [...arr].sort((a, b) => a - b);
            return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
          };
        let nilai_median_1 = median(kelas_smt_1);
        let nilai_st_dev_1 = standardDeviation(kelas_smt_1);
        let nilai_mean_1 = kelas_smt_1.reduce((a, b) => a + b) / kelas_smt_1.length;
        let total_kelas_dibawah_2_1 = 0;
        let total_kelas_2_hingga_dibawah_3_1 = 0;
        let total_kelas_3_hingga_dibawah_4_1 = 0;
        let total_kelas_4_1 = 0;
        let total_kelas_1 = 0;
        let total_nilai_indeks_1 = 0;
        kelas_smt_1.forEach(element => {
            if (element < 2) {
                total_kelas_dibawah_2_1 += 1;
            }
            else if (element < 3) {
                total_kelas_2_hingga_dibawah_3_1 += 1;
            }
            else if (element < 4) {
                total_kelas_3_hingga_dibawah_4_1 += 1;
            }
            else if (element == 4) {
                total_kelas_4_1 += 1;
            }
            total_kelas_1 += 1;
            total_nilai_indeks_1 += element;
        });
        let nilai_tertinggi_2 = Math.max(...kelas_smt_2);
        let nilai_terendah_2 = Math.min(...kelas_smt_2);
        let nilai_median_2 = median(kelas_smt_2);
        let nilai_st_dev_2 = standardDeviation(kelas_smt_2);
        let nilai_mean_2 = kelas_smt_2.reduce((a, b) => a + b) / kelas_smt_2.length;
        let total_kelas_dibawah_2_2 = 0;
        let total_kelas_2_hingga_dibawah_3_2 = 0;
        let total_kelas_3_hingga_dibawah_4_2 = 0;
        let total_kelas_4_2 = 0;
        let total_kelas_2 = 0;
        let total_nilai_indeks_2 = 0;
        kelas_smt_2.forEach(element => {
            if (element < 2) {
                total_kelas_dibawah_2_2 += 1;
            }
            else if (element < 3) {
                total_kelas_2_hingga_dibawah_3_2 += 1;
            }
            else if (element < 4) {
                total_kelas_3_hingga_dibawah_4_2 += 1;
            }
            else if (element == 4) {
                total_kelas_4_2 += 1;
            }
            total_kelas_2 += 1;
            total_nilai_indeks_2 += element;
        });
        let bodyPost = {};
        bodyPost['ips_1'] = body['ips_1'];
        bodyPost['sks_smt_1'] = body['sks_smt_1'];
        bodyPost['sks_total_1'] = body['sks_total_1'];
        bodyPost['ipk_1'] = body['ipk_1'];
        bodyPost['ips_2'] = body['ips_2'];
        bodyPost['sks_smt_2'] = body['sks_smt_2'];
        bodyPost['sks_total_2'] = body['sks_total_2'];
        bodyPost['ipk_2'] = body['ipk_2'];
        bodyPost['total_kelas_dibawah_2_1'] = total_kelas_dibawah_2_1;
        bodyPost['nilai_tertinggi_1'] = nilai_tertinggi_1;
        bodyPost['nilai_terendah_1'] = nilai_terendah_1;
        bodyPost['nilai_median_1'] = nilai_median_1;
        bodyPost['total_kelas_2_hingga_dibawah_3_1'] = total_kelas_2_hingga_dibawah_3_1;
        bodyPost['total_kelas_3_hingga_dibawah_4_1'] = total_kelas_3_hingga_dibawah_4_1;
        bodyPost['total_kelas_4_1'] = total_kelas_4_1;
        bodyPost['total_kelas_1'] = total_kelas_1;
        bodyPost['total_nilai_indeks_1'] = total_nilai_indeks_1;
        bodyPost['nilai_st_dev_1'] = nilai_st_dev_1;
        bodyPost['total_kelas_dibawah_2_2'] = total_kelas_dibawah_2_2;
        bodyPost['nilai_tertinggi_2'] = nilai_tertinggi_2;
        bodyPost['nilai_terendah_2'] = nilai_terendah_2;
        bodyPost['nilai_median_2'] = nilai_median_2;
        bodyPost['total_kelas_2_hingga_dibawah_3_2'] = total_kelas_2_hingga_dibawah_3_2;
        bodyPost['total_kelas_3_hingga_dibawah_4_2'] = total_kelas_3_hingga_dibawah_4_2;
        bodyPost['total_kelas_4_2'] = total_kelas_4_2;
        bodyPost['total_kelas_2'] = total_kelas_2;
        bodyPost['total_nilai_indeks_2'] = total_nilai_indeks_2;
        bodyPost['nilai_st_dev_2'] = nilai_st_dev_2;
        bodyPost['nilai_mean_1'] = nilai_mean_1;
        bodyPost['nilai_mean_2'] = nilai_mean_2;
        bodyPost['jk'] = body['jk'];
        let agama = body['agama'];
        if (agama == 98) {
            bodyPost['agama'] = null;
        } else if (agama == 99) {
            bodyPost['agama'] = 7;
        } else {
            bodyPost['agama'] = agama;
        }
        bodyPost['umur_saat_masuk'] = umur_saat_masuk;
        bodyPost['merantau'] = body['merantau'] == true ? 1 : 0;
        bodyPost['sp'] = body['sp'];
        let result = [];
        const response = await axios.post('http://34.82.223.138:16201/predict', {
        body: bodyPost,
        headers: {'Content-Type': 'application/json'}
        })
        .then(response => { 
            temp = {};
            temp['prediction_result'] = parseInt(response['data']['prediction_result']);
            temp['probability'] = response['data']['probability'];
            console.log(response);
            console.log(response['data']);
            result.push(temp);
            console.log(result);
            return result;
        });
        return response;
    } catch (error) {
        console.log(error);
    }
}

// async function getStatusMahasiswa(semester) {
//     try {
//         console.time('GET api/status_mahasiswa/:semester');
//         let query = `
//         SELECT sm.nm_stat_mhs, COUNT(*) AS total
//         FROM dbo.agregat_visualisasi_index AS agg
//         LEFT JOIN ref.status_mahasiswa AS sm
//         ON agg.id_stat_mhs = sm.id_stat_mhs
//         WHERE agg.id_smt = ${semester}
//         GROUP BY sm.nm_stat_mhs
//         `
//         let pool = await sql.connect(config);
//         let agregat = await pool.request()
//         .input('semester', sql.Int, semester)
//         .query(query);
//         console.timeEnd('GET api/status_mahasiswa/:semester');
//         return agregat.recordset;
//     }
//     catch (error) {
//         console.log(error);
//     }
// }

// async function getJenisKeluar(semester) {
//     try {
//         console.time('GET api/jenis_keluar/:semester');
//         let query = `
//         SELECT jk.ket_keluar, COUNT(*) AS total
//         FROM dbo.agregat_visualisasi_index AS agg
//         LEFT JOIN ref.jenis_keluar AS jk
//         ON agg.id_jns_keluar = jk.id_jns_keluar
//         WHERE agg.id_smt = ${semester}
//         GROUP BY jk.ket_keluar
//         `
//         let pool = await sql.connect(config);
//         let agregat = await pool.request()
//         .input('semester', sql.Int, semester)
//         .query(query);
//         console.timeEnd('GET api/jenis_keluar/:semester');
//         return agregat.recordset;
//     }
//     catch (error) {
//         console.log(error);
//     }
// }

// async function getIpk(semester) {
//     try {
//         let query = `
//         SELECT ipk AS ipk_1, COUNT(*) AS total
//         FROM reg_pd
//         WHERE ipk >= 3.5
//         GROUP BY ipk;
//         SELECT ipk AS ipk_2, COUNT(*) AS total
//         FROM reg_pd
//         WHERE ipk < 3.5 and ipk >= 3
//         GROUP BY ipk;
//         SELECT ipk AS ipk_3, COUNT(*) AS total
//         FROM reg_pd
//         WHERE ipk < 3 and ipk >= 2.5
//         GROUP BY ipk;
//         SELECT ipk AS ipk_4, COUNT(*) AS total
//         FROM reg_pd
//         WHERE ipk < 2.5 and ipk >= 2
//         GROUP BY ipk;
//         SELECT ipk AS ipk_5, COUNT(*) AS total
//         FROM reg_pd
//         WHERE ipk < 2
//         GROUP BY ipk;
//         `
//         let pool = await sql.connect(config);
//         let agregat = await pool.request()
//         .input('semester', sql.Int, semester)
//         .query(query);

//         return agregat.recordset;
//     }
//     catch (error) {
//         console.log(error);
//     }
// }

module.exports = {
    getSankey : getSankey,
    getSankeyNonIndex,
    // getSankeyWithIdJnsDaftar : getSankeyWithIdJnsDaftar,
    getGeo : getGeo,
    getGeoNonIndex : getGeoNonIndex,
    getBarchart : getBarchart,
    getBarchartNonIndex : getBarchartNonIndex,
    getName : getName,
    getNipd : getNipd,
    getNipdNonIndex : getNipdNonIndex,
    getIdRegPd : getIdRegPd,
    getIdRegPdNonIndex : getIdRegPdNonIndex,
    getDOPerSemester : getDOPerSemester,
    getDOPerSemesterNonIndex : getDOPerSemesterNonIndex,
    postPredict : postPredict,
    // getStatusMahasiswa : getStatusMahasiswa,
    // getJenisKeluar : getJenisKeluar,
    // getIpk : getIpk
}