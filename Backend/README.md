# PDDikti Backend

This app is using NodeJS and ExpressJS

## Installation

First, install the necessary package to run this app using the command below.

```bash
npm install
```

After that, run the app using the command below.

```bash
node app.js
```
You can check the running app in [http://localhost:8080/](http://localhost:8080/)
