# Modeling Dropout for Indonesian Undergraduate Student by Utilizing Indonesian's Higher Education Database (PDDIKTI)

This is the repository for end-to-end machine learning and visualization system for Indonesian undergraduate dropout. The project is written in "Modeling Dropout for Indonesian Undergraduate Student by Utilizing Indonesian's Higher Education Database (PDDIKTI)" by Reynard Adha Ryanda, Kevin Asyraf Putra Priyatna, and Muhammad Glend Angga Dwitama.

## Instructions

### Backend

First, install the necessary package to run this app using the command below.

```
npm install
```

After that, run the app using the command below.

```
node app.js
```

### Frontend

First, install the necessary package to run this app using the command below.

```
npm install
```

After that, run the app using the command below.

```
npm start
```

### Backend-flask

First, install the necessary package to run this app using the command below.

```
pip install -r requirements.txt
```

After that, run the app using the command below.

```
python -m flask run
```

Note: The system could only be functional by utilizing Indonesia higher education database provided by PDDIKTI.